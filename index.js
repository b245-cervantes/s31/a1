// 1. What directive is used by Node.js in loading the modules it needs?

    // Require directive

// 2. What Node.js module contains a method for server creation?

   //http module

// 3. What is the method of the http object responsible for creating a server using Node.js?

  //createServer method

// 4. What method of the response object allows us to set status codes and content types?
	
  //writeHead method

// 5. Where will console.log() output its contents when run in Node.js?

  // browser(devtool.console) or terminal

// 6. What property of the request object contains the address's endpoint?

// url? browser url?


const http = require('http');

const port = 4000;

const server = http.createServer((req,res) => {
     if(req.url == '/login'){
        res.writeHead(200,{'Content-Type':'text/plain'})
        res.end("Welcome to the login page.")
     } else {
        res.writeHead(404,{'Content-Type':'text/plain'})
        res.end("I'm sorry the page you are looking for cannot be found.")
     }
})
server.listen(4000)
console.log(`Server is running at ${port}`);